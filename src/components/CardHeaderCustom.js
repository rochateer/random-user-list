import React from "react";
import Box from '@mui/material/Box';

const CardHeaderCustom = ( props ) => {
  const styleInput = props.style !== undefined ? props.style : {};
  let styleDefaultHead = {
    margin : '10px 10px',
    fontSize : '1.5em',
    fontWeight : '600'
  }
  styleDefaultHead = {...styleDefaultHead, ...styleInput}
  return (
    <Box sx={styleDefaultHead} align="right">
        {props.children}
    </Box>
  )
};

export default CardHeaderCustom;