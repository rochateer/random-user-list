import * as React from 'react';
import PropTypes from 'prop-types';
import Stack from '@mui/material/Stack';
import Button from '@mui/material/Button';
import Snackbar from '@mui/material/Snackbar';
import MuiAlert from '@mui/material/Alert';
import {useDispatch, useSelector} from 'react-redux';

const Alert = React.forwardRef(function Alert(props, ref) {
  return <MuiAlert elevation={6} ref={ref} variant="filled" {...props} />;
});

export default function AlertNotification(props) {
  const [open, setOpen] = React.useState(false);

  const dispatch = useDispatch()

  const handleClick = () => {
    setOpen(true);
  };

  const handleClose = (event, reason) => {
    if (reason === 'clickaway') {
      return;
    }
    dispatch({ type: 'DEL_NOTIFY' })
    setOpen(false);
  };

  const alertMessage = (status, message) => {
    switch(status){
      case "success":
        return message !== null && message !== undefined ? message : "Your action was successful"
      case "error":
        return message !== null && message !== undefined ? message : "There is something error, please try again"
      case "warning":
        return message !== null && message !== undefined ? message : "Its look there is a little trouble here, please try again or if your action was succesful just let it go";
      case "info":
        return message !== null && message !== undefined ? message : "Hey!!!";
      default: return "None, just leave it"
    }
  }

  React.useEffect(() => {
    if(props.alert_status){
      handleClick();
    }
  }, [open]);

  return (
    <React.Fragment>
      {props.alert_status !== undefined && props.alert_status !== null && (
        <Stack spacing={2} sx={{ width: '100%' }}>
          <Snackbar open={true} onClose={handleClose} anchorOrigin={{ vertical : 'top', horizontal : 'right'}}>
            <Alert onClose={handleClose} severity={props.alert_status.toLowerCase()} sx={{ width: '100%' }}>
              {alertMessage(props.alert_status.toLowerCase(), props.alert_message)}
            </Alert>      
          </Snackbar>
        </Stack>
      )}
    </React.Fragment>
  );
}

AlertNotification.propTypes = {
  alert_message: PropTypes.string,
  alert_status: PropTypes.string
};