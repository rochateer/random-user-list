import { useTheme, styled } from '@mui/material/styles';
import TableCell, { tableCellClasses } from '@mui/material/TableCell';

const StyledTableCellHeader = styled((props) => (<TableCell {...props}/>))(({ theme }) => ({
    [`&.${tableCellClasses.head}`]: {
      backgroundColor: theme.palette.common.black,
      color: theme.palette.common.white,
      textAlign : 'center'
    }
  }));

export default StyledTableCellHeader;