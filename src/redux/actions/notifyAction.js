import {LOADING_PRC,SHOW_NOTIFY} from '../types/actInTypes'

export const notificationFlag = (data) => dispatch => {
    let action_messsage = null;
    if(data.action_message){
        if(typeof data.action_message === 'object'){
            if(data.action_message.response !== undefined && data.action_message.response.data !== undefined && data.action_message.response.data.message){
                action_messsage = data.action_message.response.data.message
            }
        }else{
            action_messsage = data.action_message
        }
    }
    dispatch( {
        type: SHOW_NOTIFY,
        payload: {
            action_status : data.action_status,
            action_msg : action_messsage
        }
    })
}

export const loadingTrue = () => dispatch => {
    dispatch( {
        type: SHOW_NOTIFY,
        payload: {
            action_status : null,
            action_msg : null
        }
    })
    dispatch( {
        type: LOADING_PRC,
        loading_status: true
    })
}

export const loadingFalse = () => dispatch => {
    dispatch( {
        type: LOADING_PRC,
        loading_status: false
    })
}