import { combineReducers } from 'redux'
import notifyReducer from './notifyReducer'

const rootReducer = combineReducers({
  notificationAlert : notifyReducer
})

export default rootReducer