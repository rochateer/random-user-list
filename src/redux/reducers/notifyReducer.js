import {SHOW_NOTIFY, DEL_NOTIFY, LOADING_PRC} from '../types/actInTypes'

const initialState = {
    action_status : null,
    action_msg : null,
    loading_status : false,
}

export default function notifyReducer(state = initialState, action){
    switch(action.type){

        case SHOW_NOTIFY:
            return {
                ...state,
                ...action.payload
            }
        case DEL_NOTIFY:
            return {
                ...state,
                action_status : null,
                action_msg : null
            }
        case LOADING_PRC:
            return {
                ...state,
                loading_status : action.loading_status
            }
        default: return state
    }

}