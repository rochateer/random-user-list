import * as React from 'react';

import Box from '@mui/material/Box';
import Table from '@mui/material/Table';
import TableHead from '@mui/material/TableHead';
import TableBody from '@mui/material/TableBody';
import TableCell, { tableCellClasses } from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableFooter from '@mui/material/TableFooter';
import TableRow from '@mui/material/TableRow';
import Button from '@mui/material/Button';
import { Card, CardContent, Grid, TextField, Autocomplete } from '@mui/material';
import {useDispatch} from 'react-redux';
import Pagination from '@mui/material/Pagination';
import Skeleton from '@mui/material/Skeleton';

import debounce from 'lodash.debounce'

import CardHeaderCustom from '../../components/CardHeaderCustom';
import StyledTableCellHeader from '../../components/styledTableCellHeader';

import { getDatafromAPI } from "../../helper/asyncHelper";

import { convertDateFormatfull } from "../../helper/basicHelper";

import {notificationFlag} from '../../redux/actions/notifyAction'

const gender_option = [
  { label: 'None', value: '' },
  { label: 'Female', value: 'female' },
  { label: 'Male', value: 'male' }
];

const sort_option = [
  { label: 'None', sortBy: '', sortOrder : '' },
  { label: 'User Name (ASC)', sortBy: 'username', sortOrder : 'asc' },
  { label: 'User Name (DSC)', sortBy: 'username', sortOrder : 'dsc' },
  { label: 'Name (ASC)', sortBy: 'name', sortOrder : 'asc' },
  { label: 'Name (DSC)', sortBy: 'name', sortOrder : 'dsc' },
  { label: 'Email (ASC)', sortBy: 'email', sortOrder : 'asc' },
  { label: 'Email (DSC)', sortBy: 'email', sortOrder : 'dsc' },
  { label: 'Gender (ASC)', sortBy: 'gender', sortOrder : 'asc' },
  { label: 'Gender (DSC)', sortBy: 'gender', sortOrder : 'dsc' },
  { label: 'Register Date (ASC)', sortBy: 'registered', sortOrder : 'asc' },
  { label: 'Register Date (DSC)', sortBy: 'emailregistered', sortOrder : 'dsc' }, 
];


export default function ListUser() {
  const [activePage, setActivePage] = React.useState(1);
  const [rowsPerPage, setRowsPerPage] = React.useState(10);
  const [totalPage, setTotalPage] = React.useState(10);
  const [dataTotal, setDataTotal] = React.useState(200);
  const [pageTotal, setPageTotal] = React.useState(20);
  const [dataList, setDataList] = React.useState([]);
  const [valuesFilter, setValuesFilter] = React.useState({keyword : '', gender : ''})
  const [valuesSort, setValuesSort] = React.useState({sortBy : '', sortOrder : ''})
  const [page, setPage] = React.useState(0);
  const [onLoading, setOnLoading] = React.useState(false);

  const dispatch = useDispatch();

  const debounceFilterData = React.useCallback(debounce((filterValue, sortValue) => getDataList(filterValue, sortValue), 1000), []);

  const getDataList = async(filterValue, sortValue) => {
    setOnLoading(true)
    let filter_array = []
    let whereAnd = ''
    let sort_api = ''
    let filter_value = valuesFilter
    let sort_value = valuesSort
    if(filterValue !== undefined){
      filter_value = filterValue
    }
    if(filter_value !== undefined){
      for(let label in filter_value){
        if(filter_value[label]){
          filter_array.push(label+'=' + filter_value[label]);
        }
      }
      whereAnd = filter_array.join('&');
    }
    if(sortValue){
      sort_value = sortValue
    }
    if(sort_value){
      if(sort_value.sortBy){
        sort_api = '&sortBy='+sort_value.sortBy+'&sortOrder='+sort_value.sortOrder
      }
    }
    const res = await getDatafromAPI('?page='+activePage+'&results='+rowsPerPage+sort_api+'&'+whereAnd)
    console.log(res.data.results)
    if(res !== undefined && res.data !== undefined){
      setDataList(res.data.results)
    }else{
      if(res !== undefined){
        dispatch(notificationFlag({action_status : 'error', action_message : res.response.error}))
      }else{
        dispatch(notificationFlag({action_status : 'error', action_message : null}))
      }
    }
    setOnLoading(false)
  }

  React.useEffect(() => {
    getDataList();
  }, []);

  const resetFilter = () => {
    setValuesFilter({keyword : '', gender : ''});
    debounceFilterData({}, valuesSort)
  }

  const handleChangeSortingACValues = (name, value, valuesFilter) => {
    const selectedsortBy = value !== undefined && value !== null ? value.sortBy : null;
    const selectedsortOrder = value !== undefined && value !== null ? value.sortOrder : null;
    let formSort = {sortBy : selectedsortBy, sortOrder : selectedsortOrder}
    setValuesSort(formSort);
    getDataList(valuesFilter, formSort)
  }

  const handleChangeSelectACValues = (name, value, valuesFilter) => {
    const selectedValue = value !== undefined && value !== null ? value.value : null;
    let formFilter = { ...valuesFilter, [name]: selectedValue }
    setValuesFilter(formFilter);
    debounceFilterData(formFilter, valuesSort)
  }

  const handleInputFilter = (name, valuesFilter) => (event) => {
    let formFilter = {...valuesFilter,[name] : event.target.value}
    setValuesFilter(formFilter);
    debounceFilterData(formFilter, valuesSort)
  };

  const handleChangePage = (event, newPage) => {
    setActivePage(newPage);
  };

  React.useEffect(() => {
    getDataList();
  }, [activePage])

  return (
    <Box>
      
      <Card sx={{ margin: '20px 20px' }}>
        <CardHeaderCustom>
          <div>
            User List
          </div>          
        </CardHeaderCustom>
        <CardContent>
          <div className="random-user-search__row">
            <Box>
              <Grid container spacing={1} sx={{alignItems: 'center'}}>
                <Grid item xs={12} md={2}>
                  <div>
                    <TextField value={valuesFilter["keyword"]} id="Search Keyword" name="keyword" onChange={handleInputFilter("keyword", valuesFilter)} size="small" label="Search Keyword" variant="outlined" />
                  </div>
                </Grid>
                <Grid item xs={8} md={2}>
                  <div>
                    <Autocomplete
                      disablePortal
                      name="gender"
                      id="gender"
                      value={valuesFilter["gender"]}
                      options={gender_option}
                      disableClearable
                      onChange={(event, value) => handleChangeSelectACValues("gender", value, valuesFilter)}
                      renderInput={(params) => <TextField {...params} id="Search Gender" size="small" label="Search Gender" variant="outlined" />}
                    />
                  </div>
                </Grid>
                <Grid item xs={2} md={6}>
                  <div>
                    <Button size="small" onClick={resetFilter}>Reset</Button>
                  </div>
                </Grid>
                <Grid item xs={12} md={2} alignItems="flex-end">
                  <div>
                    <Autocomplete
                      sx={{marginRight:'20px'}}
                      disablePortal
                      name="sort_by"
                      id="sort_by"
                      value={valuesFilter["sort_by"]}
                      options={sort_option}
                      disableClearable
                      onChange={(event, value) => handleChangeSortingACValues("sort_by", value)}
                      renderInput={(params) => <TextField {...params} id="sort_by" size="small" label="Sort By" variant="outlined" />}
                    />
                  </div>
                </Grid>
              </Grid>
            </Box>
            <Box>
              <div>
                
              </div>
            </Box>
          </div>
          <TableContainer sx={{ maxHeight: 440 }} >
            <Table stickyHeader aria-label="sticky table">
              <TableHead>
                <TableRow>
                  <StyledTableCellHeader>User Name</StyledTableCellHeader>
                  <StyledTableCellHeader >Name</StyledTableCellHeader>
                  <StyledTableCellHeader >Email</StyledTableCellHeader>
                  <StyledTableCellHeader >Gender</StyledTableCellHeader>
                  <StyledTableCellHeader >Registered Date</StyledTableCellHeader>
                </TableRow>
              </TableHead>
              
              {!onLoading && (
                <React.Fragment>
                  <TableBody>
                  {dataList.map((row) => (
                    <TableRow key={row.Id}>
                      <TableCell component="th" scope="row">
                        {row.login.username}
                      </TableCell>
                      <TableCell align="left">
                        {row.name ? row.name.first+" "+row.name.last : null}
                      </TableCell>
                      <TableCell align="left">
                        {row.email}
                      </TableCell>
                      <TableCell align="center">
                        {row.gender}
                      </TableCell>
                      <TableCell align="center">
                        {convertDateFormatfull(row.registered.date)}
                      </TableCell>
                    </TableRow>
                  ))}
                </TableBody>
              </React.Fragment>
              )}
              
            </Table>
          </TableContainer>
          {!onLoading ? (
            <div className="random-user-pagination-container">
              <Pagination className="random-user-pagination-container__pagination" size="small" count={parseInt(pageTotal)} showFirstButton showLastButton onChange={handleChangePage}/>
              <span className="random-user-pagination-container__total-data-info">Showing {dataList.length} of {dataTotal} data</span>
            </div>
          ) : (
            <div><Skeleton animation="wave" sx={{ bgcolor: 'grey.500' }} /></div>
          )}
        </CardContent>
      </Card>
    </Box>
  );
}
