import axios from "axios";

const API_URL = "https://randomuser.me/api/"

export const getDatafromAPI = async (url) => {
  //put another params API in "url"
  try {
    let respond = await axios.get(API_URL + url, {
      headers: { "Content-Type": "application/json" }
    });
    return respond;
  } catch (err) {
    let respond = err;
    return respond;
  }
}

export const postDatatoAPI = async (url, data) => {
  try {
    let respond = await axios.post(API_URL + url, data, {
      headers: {
        "Content-Type": "application/json"
      },
    });
    return respond;
  } catch (err) {
    let respond = err;
    return respond;
  }
};

export const deleteDatafromAPI = async (url, data) => {
  try {
    let respond = await axios.delete(API_URL + url, {
      headers: { 
        "Content-Type": "application/json" 
      },
      data: data
    });
    return respond;
  } catch (err) {
    let respond = err;
    return respond;
  }
}