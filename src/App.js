import React from 'react'
import logo from './logo.svg';
import './App.css';
import './scss/style.scss';
import { Route, Switch, BrowserRouter,Redirect } from 'react-router-dom';
import LoaderPage from './components/loaderPage';
import Box from '@mui/material/Box';

import {useDispatch, useSelector} from 'react-redux';

import AlertNotification from './components/notificationAlert';

const UserList = React.lazy(() => import('./views/user/listUser'));

function App() {
  
  const notifyAlert = useSelector(state => state.notificationAlert)

  return (
    <Box>
      <AlertNotification alert_status={notifyAlert.action_status} alert_message={notifyAlert.action_msg} />
      <BrowserRouter>
        <React.Suspense fallback={<LoaderPage />}>
          <Switch>
            <Route path="/" name="User List" render={props => <UserList dataRoute={props} />} />
          </Switch>
        </React.Suspense>
      </BrowserRouter>
    </Box>
  );
}

export default App;
