# User Management

Website for show user data

## Development
The website development using : 

- React : "^17.0.2"
- Material UI : "^5.0.5"
- Axios : "^0.24.0"
- Redux : "^4.1.2"
- Redux-thunk : "^2.4.1"
- React-router-dom : "^5.3.0"

1. Clone the repository and move into:
```bash
git clone https://gitlab.com/rochateer/random-user-list.git
```
you can change the folder name by your self

2. Install:
```bash
npm Install
```

3. Start the project
```bash
npm start
```

4. If you wanna build for production
```bash
npm run build
```

### Development (Code)

#### User List

using API from https://randomuser.me/api/

#### View
if you wanna change or adding view go to the **\src\views**.

#### Component
All of default component like notification banner or etc inside folder **\src\components**

#### Helper
Helper or Default function is place you will put all of function will be able in everywhere, so yo dont need to rewrite same function for same purpose, the folder is **\src\helper**, file __asyncHelper.js__ is palce for asynchronous method and __basicHelper.js__ is place for method other than that

#### Styling
for adding some additional style or change current style, the folder is **\src\scss** and the main file for styling is ___custom.scss__

#### Redux
for adding some redux process go to the **\src\redux** this website also support redux-thunk

